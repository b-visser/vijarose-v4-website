@extends('layouts.default')

@section('title', 'Order history')

@section('content')
    <div class="card">
        <div class="card-header">
            @yield('title')
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th class="text-right">Date</th>
                </tr>
                </thead>
                <tbody>
                @forelse($orders as $key => $order)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $order->package->name }}</td>
                        <td>{{ currencyFormat($order->package->price) }}</td>
                        <td>{{ $order->status }}</td>
                        <td class="text-right">{{ $order->package->created_at->format('Y-m-d') }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">No results...</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        @if($orders->count() > 10)
            <div class="card-footer">
                {{ $orders->links() }}
            </div>
        @endif
    </div>
@endsection

@section('styles')
    <style>
        .pagination {
            justify-content: center !important;
        }
    </style>
@endsection
