@extends('layouts.default')

@section('title', 'Announcements')

@section('content')
    <div class="card">
        <header class="card-header">
            <h1>@yield('title')</h1>
        </header>
        <div class="card-body">
            <header>
                <h2>Now launching!</h2>
                <aside>
                    <small class="date">3 January 2019</small>
                </aside>
            </header>
            <p>
                Welcome to Vija R.O.S.E. Online, a medium-low rate server.<br>
                We are currently in an early release with a few bugs but we work on solving these on a daily basis.
            </p>
        </div>
        <footer class="card-footer">

        </footer>
    </div>
@endsection
