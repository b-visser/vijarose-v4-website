<?php

namespace App\Http\Composers;

use App\Models\VoteSite;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class VoteSiteComposer
{
    protected $voteSites;

    public function __construct()
    {
        $this->voteSites = VoteSite::all();
    }

    public function compose(View $view)
    {
        if (Auth::check()) {
            $view->with('voteSites', $this->voteSites);
        }
    }
}
