<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use App\Models\Account;

class ServerSettingsComposer
{
    protected $settings;

    public function __construct()
    {
        DB::transaction(function () {
            $this->settings = DB::select('select exp_rate, drop_rate, zuly_rate FROM list_config');
		
            $this->settings[0]->online = Account::where('online', 1)->count();
        });
    }

    public function compose(View $view)
    {
		$view->with('settings', $this->settings);
    }
}
