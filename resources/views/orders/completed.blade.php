@extends('layouts.default')

@section('title', 'Order placed')

@section('content')
    <div class="card">
        <div class="card-header">
            @yield('title')
        </div>
        <div class="card-body">
            @if($order->status !== 'completed')
                Your order has been placed. but the payment was not yet received.<br>
                Please keep this page open. We will check your transaction every 30 seconds!
            @else
                Your order has been completed. and the points have been added to your account!<br>
                We thank you for your patronage!
            @endif
        </div>
    </div>
@endsection

@if($order->status !== 'completed')
    @section('scripts')
        <script>
            setInterval(function () {
                location.href = '{{ route('order.status') }}?tx={{ $order->transaction_id }}&item_number={{ $order->id }}';
            }, 30000);
        </script>
    @endsection
@endif
