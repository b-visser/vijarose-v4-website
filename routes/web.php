<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('announcements');
})->name('announcements');

Route::get('login', ['as' => 'guest.login', 'uses' => 'AuthController@login']);
Route::post('login', ['as' => 'guest.login', 'uses' => 'AuthController@authenticate']);

Route::get('register', ['as' => 'guest.register', 'uses' => 'AccountController@create']);
Route::post('register', ['as' => 'guest.register', 'uses' => 'AccountController@store']);

Route::get('order/notification', ['as' => 'order.notification', 'uses' => 'OrderController@notification']);

Route::group(['middleware' => ['auth']], function () {
    Route::any('logout', ['as' => 'auth.logout', 'uses' => 'AuthController@logout']);
    Route::get('vote/{site}', ['as' => 'vote.site', 'uses' => 'VoteController@redirect']);

    Route::group(['prefix' => 'order', 'as' => 'order.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'OrderController@order']);
        Route::post('/', ['as' => 'index', 'uses' => 'OrderController@postOrder']);
        Route::get('history', ['as' => 'history', 'uses' => 'OrderController@history']);
        Route::get('status', ['as' => 'status', 'uses' => 'OrderController@status']);
        Route::get('completed/{txn}', ['as' => 'completed', 'uses' => 'OrderController@completed']);
        Route::get('canceled/{order}', ['as' => 'canceled', 'uses' => 'OrderController@canceled']);
    });
});
