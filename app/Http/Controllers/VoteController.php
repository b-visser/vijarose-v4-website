<?php

namespace App\Http\Controllers;

use App\Models\Vote;
use App\Models\VoteSite;
use Illuminate\Support\Facades\Auth;

class VoteController extends Controller
{
    public function redirect($voteSiteId)
    {
        $voteSite = VoteSite::find($voteSiteId);

        if (is_null($voteSite)) {
            return redirect()
                ->back()
                ->withErrors(['This vote site does not exists!']);
        }

        $account = Auth::user();
        $vote = $account->votes()->where('vote_site_id', $voteSite->id)->first();

        if (is_null($vote)) {
            $vote = Vote::create([
                'account_id' => $account->id,
                'vote_site_id' => $voteSite->id,
                'time' => 0,
            ]);
        }

        if ($vote->time < time()) {

            $newVoteTime = time() + ($voteSite->interval * 3600);
            $vote->time = $newVoteTime;
            $vote->update();

            $account->itemmall_points += $voteSite->reward;
            $account->update();
        }

        return redirect($voteSite->url);
    }
}
