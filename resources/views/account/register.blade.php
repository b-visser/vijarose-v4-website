@extends('layouts.default')

@section('title', 'Registration')

@section('content')
    <div class="card">
        <header class="card-header">
            <h1>@yield('title')</h1>
        </header>
        <form action="{{ route('guest.register') }}" method="post">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <label for="username">Username:</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username..." value="{{ old('username') }}">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email..." value="{{ old('email') }}">
                </div>
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password...">
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Confirm password:</label>
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Password confirmation...">
                </div>
            </div>
            <footer class="card-footer text-right">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </footer>
        </form>
    </div>
@endsection
