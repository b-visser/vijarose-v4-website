<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Models\Package;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

define('PAYPAL_SANDBOX', 'https://www.sandbox.paypal.com');
define('PAYPAL_SANDBOX_AUTH', 'vuhnqc6jYvI5CsEnxf5VwYHneM9hX_xuLIlFKkWT4fHaMxbhqork6v1UfFK');
define('PAYPAL_PRODUCTION', 'https://www.paypal.com');
define('PAYPAL_PRODUCTION_AUTH', '');

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{
    /** @var string $paypalEnvironment */
    private $paypalEnvironment;

    /** @var string $paypalAuth */
    private $paypalAuth;

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->paypalEnvironment = PAYPAL_PRODUCTION;
        $this->paypalAuth = PAYPAL_PRODUCTION_AUTH;

        if (env('APP_DEBUG') === true) {
            $this->paypalEnvironment = PAYPAL_SANDBOX;
            $this->paypalAuth = PAYPAL_SANDBOX_AUTH;
        }
    }

    /**
     * @return Factory|View
     */
    public function order()
    {
        $packages = Package::active()->get();

        return view('orders.checkout', compact('packages'));
    }

    /**
     * @param OrderRequest $request
     * @return RedirectResponse
     */
    public function postOrder(OrderRequest $request)
    {
        if (null === $package = Package::find($request->input('package'))) {
            throw new \RuntimeException('Package not found');
        }

        $order = Order::create([
            'status' => 'pending',
            'account_id' => Auth::user()->id,
            'package_id' => $package->id,
            'total' => $package->price,
        ]);

        $params = http_build_query([
            'cmd' => '_donations',
            'business' => 'bjornvisser-facilitator@live.nl',
            'item_name' => $package->name,
            'amount' => $package->price,
            'cancel_return' => route('order.canceled', $order->id),
            'currency_code' => 'USD',
            'item_number' => $order->id,
        ]);

        return Redirect::away($this->paypalEnvironment . '?' . $params);
    }

    /**
     * @param string $txn
     * @return Factory|View
     */
    public function completed($txn)
    {
        $order = Order::where('transaction_id', $txn)->first();

        if (null === $order) {
            throw new \RuntimeException('Order #'. $txn .' not found');
        }

        return view('orders.completed', compact('order'));
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function status(Request $request)
    {
        $order = Order::find($request->input('item_number'));

        if (null === $order) {
            throw new \RuntimeException('Order not found');
        }

        $client = new Client([
            'base_uri' => $this->paypalEnvironment,
            'verify' => false,
        ]);

        try {
            $request = $client->post('/cgi-bin/webscr', [
                'form_params' => [
                    'tx' => $request->input('tx'),
                    'at' => $this->paypalAuth,
                    'cmd' => '_notify-synch',
                ],
            ]);
        } catch (RequestException $e) {
            header('Content-type: text/html;');
            print($e->getMessage());
            die();
        }

        $params = [];
        $result = explode("\n", $request->getBody()->getContents());

        if (strtolower($result[0]) === 'success') {
            unset($result[0]);
            foreach ($result as $item) {
                $keyVal = explode('=', $item, 2);

                if (array_key_exists('1', $keyVal)) {
                    $params[urldecode($keyVal[0])] = urldecode($keyVal[1]);
                }
            }

            $order->transaction_id = $params['txn_id'];

            if (strtolower($params['payment_status']) === 'completed') {
                $order->status = strtolower($params['payment_status']);

                $account = $order->account;
                $account->itemmall_points += ($order->package->points + calcBonus($order->package->points, $order->package->bonus_percentage));
                $account->save();
            }

            $order->save();
        }

        return redirect()->route('order.completed', $order->transaction_id);
    }

    /**
     * @return Factory|View
     */
    public function canceled(Order $order)
    {
        $order->status = 'canceled';
        $order->save();

        return redirect()->route('order.index');
    }

    public function history()
    {
        $orders = Auth::user()->orders()->paginate();

        return view('orders.history', compact('orders'));
    }
}
