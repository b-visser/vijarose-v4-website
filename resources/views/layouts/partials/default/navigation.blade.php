<div class="card card-navigation">
    <header class="card-header">
        @if(Auth::check())
            Welcome, {{ Auth::user()->username }}!
        @else
            Welcome!
        @endif
    </header>
    <div class="card-body">
        <nav class="nav flex-column nav-pills">
            <a class="nav-link {{ (Route::currentRouteName() === 'announcements' ? 'active bg-secondary' : '') }}" href="{{ route('announcements') }}">News</a>
            <a class="nav-link" target="_blank" href="https://mega.nz/#!7ts1xYZb!uH9WEda87xoqP0JirC9nenLXe_4i9n23OI_1A2yzhLs">Download</a>
            <a class="nav-link" target="_blank" href="https://discord.gg/GcgTrJu">Discord</a>

            @if(!Auth::check())
                <a class="nav-link {{ (Route::currentRouteName() === 'guest.register' ? 'active bg-secondary' : '') }}" href="{{ route('guest.register') }}">Register</a>
                <a class="nav-link {{ (Route::currentRouteName() === 'guest.login' ? 'active bg-secondary' : '') }}" href="{{ route('guest.login') }}">Login</a>
            @endif


            @if(Auth::check())
                <a class="nav-link {{ (Route::currentRouteName() === 'auth.logout' ? 'active bg-secondary' : '') }}" href="{{ route('auth.logout') }}">Logout</a>
            @endif
        </nav>
    </div>
</div>

@if(Auth::check())
    <div class="card mt-3">
        <header class="card-header">
            Vote For Points
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <p>
                        Points: {{ Auth::user()->itemmall_points }}
                    </p>

                    @foreach ($voteSites as $voteSite)
                        <p>
                            <a class="btn btn-primary btn-block" href="{{ route('vote.site', [$voteSite->id]) }}" target="_blank">
                                {{ $voteSite->name }}
                            </a>
                        </p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
