@extends('layouts.default')

@section('title', 'Checkout')

@section('content')
    <div class="card">
        <div class="card-header">
            @yield('title')
        </div>
        <form action="{{ route('order.index') }}" method="post">
            {{ csrf_field() }}
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>points</th>
                            <th class="text-right">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($packages as $package)
                        <tr>
                            <td>
                                <label for="package-{{ $package->id }}" class="sr-only">Package {{ $package->id }}</label>
                                <input type="radio" @if((int)$package->price === 10) checked="checked" @endif name="package" id="package-{{ $package->id }}" value="{{ $package->id }}">
                            </td>
                            <td>{{ $package->name }}</td>
                            <td>
                                {{ format($package->points) }}
                                @if($package->bonus_percentage)
                                    <span class="badge badge-success">
                                        +{{ format(calcBonus($package->points, $package->bonus_percentage)) }}
                                    </span>
                                @endif
                            </td>
                            <td class="text-right">{{ currencyFormat($package->price) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4">
                                <div class="form-check">
                                    <input id="confirmation" name="confirmation" type="radio" required="required" class="form-check-input">
                                    <label for="confirmation" class="form-check-label"><small>I agree to terms of service</small></label>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary">
                    Checkout
                </button>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $('tr').on('click', function () {
            $(this).find('[type="radio"]').prop('checked', true);
        });
    </script>
@endsection
