<div class="card">
    <header class="card-header">
        Server Info
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <table class="table table-sm">
				
                    @foreach($settings as $data)
						@foreach($data as $key => $setting)
							<tr>
								<td>{{ trans('settings.' . $key) }}</td>
								<td>{{ $setting }}</td>
							</tr>
						@endforeach                        
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

