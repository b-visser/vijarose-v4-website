<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'account_id',
        'package_id',
        'transaction_id',
        'total',
        'status',
    ];

    protected $perPage = 15;

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }
}
