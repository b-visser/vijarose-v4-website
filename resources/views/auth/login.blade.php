@extends('layouts.default')

@section('title', 'Login')

@section('content')
    <div class="card">
        <header class="card-header">
            <h1>@yield('title')</h1>
        </header>
        <form action="{{ route('guest.login') }}" method="post">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <label for="username">Username:</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username..." value="{{ old('username') }}">
                </div>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password..." value="{{ old('password') }}">
                </div>
            </div>
            <footer class="card-footer text-right">
                <button type="submit" class="btn btn-primary">
                    Login
                </button>
            </footer>
        </form>
    </div>
@endsection
