<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ env('APP_NAME') }} - @yield('title')</title>

    @if(App::environment('production'))
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131687328-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-131687328-1');
    </script>
    @endif

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style>
        .content-row {
            position: relative
        }

        .content-row:before {
            content: "";
            position: absolute;
            width: 160px;
            height: 500px;
            background: url({{ asset('images/dealer.png') }}) top center no-repeat / contain;
            left: -90px;
            z-index: 1;
        }

        .content-row:after {
            content: "";
            position: absolute;
            width: 160px;
            height: 500px;
            background: url({{ asset('images/muse.png') }}) top center no-repeat / contain;
            top: 32px;
            right: -90px;
            z-index: 1;
        }

        .card-header {
            text-align: center;
            font-weight: bold;
        }
    </style>
    @yield('styles')
</head>
<body>

<div class="container">
    <div class="row justify-content-center mt-3 content-row">
        <div class="col-3">
            @include('layouts.partials.default.navigation')
        </div>
        <div class="col-6">
            @include('layouts.partials.common')
            @yield('content')
        </div>
        <div class="col-3">
            @include('layouts.partials.default.aside')
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
@yield('scripts')
</body>
</html>
