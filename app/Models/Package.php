<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'name',
        'points',
        'extra_points',
        'price',
        'active',
        'purchase_count',
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
