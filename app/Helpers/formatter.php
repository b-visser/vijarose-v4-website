<?php

define('CURRENCY', '$');

function format($value, $decimals = 0)
{
    return number_format($value, $decimals, ',', '.');
}

function currencyFormat($value)
{
    return CURRENCY . ' ' . format($value, 2);
}

function calcBonus($points, $percentage)
{
    $bonusPoints = ($points / 100) * $percentage;

    return $bonusPoints;
}
