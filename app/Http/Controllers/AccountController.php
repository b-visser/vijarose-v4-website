<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateRequest;
use App\Models\Account;

class AccountController extends Controller
{
    public function create()
    {
        return view('account.register');
    }

    public function store(CreateRequest $request)
    {
        Account::create($request->all());

        return redirect()
            ->route('announcements')
            ->withMessage('Registration completed, you can now login!');
    }
}
