<?php

use App\Models\Package;
use Illuminate\Database\Seeder;

class PackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $packages = [
            [
                'name' => 'Package 1',
                'points' => 500,
                'price' => 5,
            ],
            [
                'name' => 'Package 2',
                'points' => 1000,
                'price' => 10,
                'bonus_percentage' => 5,
            ],
            [
                'name' => 'Package 3',
                'points' => 2500,
                'price' => 25,
                'bonus_percentage' => 7,
            ],
        ];

        foreach ($packages as $package) {
            Package::firstOrCreate($package);
        }
    }
}
