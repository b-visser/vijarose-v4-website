<?php

namespace App\Providers;

use App\Http\Composers\ServerSettingsComposer;
use App\Http\Composers\VoteSiteComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', VoteSiteComposer::class);
        View::composer('*', ServerSettingsComposer::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
