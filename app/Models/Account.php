<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Account extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'username',
        'password',
        'itemmall_points',
        'email',
    ];

    public $timestamps = false;

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = md5($value);
    }

    public function setRememberTokenAttribute($value)
    {
        // todo: we dont use this fucker.
//        $this->attributes['remember_token'] = $value;
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
