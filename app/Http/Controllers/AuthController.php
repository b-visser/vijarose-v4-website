<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\AuthenticateRequest;
use App\Models\Account;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function authenticate(AuthenticateRequest $request)
    {
        $account = Account::where('username', $request->username)
            ->where('password', md5($request->input('password')))
            ->first();

        if (null === $account) {
            return back()
                ->exceptInput('password')
                ->withErrors(['Username or password invalid']);
        }

        Auth::login($account);

        return redirect()
            ->route('announcements')
            ->withMessage('You are now logged in');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('announcements');
    }
}
