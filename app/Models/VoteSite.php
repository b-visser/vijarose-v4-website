<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoteSite extends Model
{
    protected $fillable = [
        'name',
        'url',
        'logo',
        'reward',
        'reward_callback',
        'interval',
    ];

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }
}
