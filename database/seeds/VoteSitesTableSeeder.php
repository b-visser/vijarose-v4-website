<?php

use App\Models\VoteSite;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VoteSitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('votes')->truncate();
        DB::table('vote_sites')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        VoteSite::create([
            'name' => 'XtremeTop100',
            'url' => 'http://www.xtremetop100.com/in.php?site=1132365880',
            'logo' => 'xtremetop100',
        ]);

        VoteSite::create([
            'name' => 'GTop100',
            'url' => 'https://gtop100.com/topsites/Rose-Online/sitedetails/Vija-ROSE-Online-94534?vote=1',
            'logo' => 'gtop100',
        ]);
    }
}
