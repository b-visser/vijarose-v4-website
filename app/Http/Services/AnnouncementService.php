<?php

namespace App\Http\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

define('WEBHOOK_NEWS', '530414749729423380/C8K1ewAQPxsOqi6efdLoTIOyUGS1qRq-tkDbJCRkQdQS5COHOEBlnj6hrOOPDnoH12uw');
define('WEBHOOK_EVENTS', '530414772026474497/jPfVLDtY3P-E4ht9N1PgmsTunH2-nobb3bUpU0yuDkZE3M4Sf2Uprwp3j6YMTSRTtekk');

class AnnouncementService
{
    public static function send($hook, array $announcement = [])
    {
        $client = new Client([
            'headers' => [
                'Accept' => 'Application/json',
                'Content-type' => 'Application/json',
            ],
        ]);
        $url = 'https://discordapp.com/api/webhooks/';
        try {
            $client->request('POST', $url . $hook, [
                'body' => json_encode($announcement),
            ]);
        } catch (RequestException $e) {
            dd($e->getMessage());
        }
    }
}
