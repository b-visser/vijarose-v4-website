<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'account_id',
        'vote_site_id',
        'time',
    ];

    public $timestamps = false;

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function voteSite()
    {
        return $this->belongsTo(VoteSite::class);
    }
}
